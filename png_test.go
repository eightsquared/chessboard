package chessboard

import (
	"fmt"
	"io/ioutil"
	"testing"

	"gitlab.com/eightsquared/chess"
)

func BenchmarkWritePNG(b *testing.B) {
	positions := make([]chess.Game, 1e5)
	for i := range positions {
		positions[i] = chess.RandomGame(i)
	}
	cases := []struct {
		size int
	}{
		{500},
		{250},
	}
	test := func(i int) func(*testing.B) {
		size := cases[i].size
		options := Defaults
		options.Size = size
		return func(b *testing.B) {
			var err error
			for i := 0; i < b.N; i++ {
				err = WritePNG(ioutil.Discard, positions[i%len(positions)], options)
				if err != nil {
					b.Fatal(err)
				}
			}
		}
	}
	for i, c := range cases {
		b.Run(fmt.Sprintf("%dpx", c.size), test(i))
	}
}

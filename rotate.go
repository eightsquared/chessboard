package chessboard

import (
	"gitlab.com/eightsquared/chess"
)

// rotate returns the top-left coordinate of the square s when the board is
// rotated by the rotation r and each rendered square is of the given size.
func rotate(s chess.Square, r Rotation, size int) (x, y int) {
	file, rank := s.File(), s.Rank()
	switch r {
	default:
		x, y = int(file-'a')*size, int('8'-rank)*size
	case Clockwise:
		x, y = int(rank-'1')*size, int(file-'a')*size
	case CounterClockwise:
		x, y = int('8'-rank)*size, int('h'-file)*size
	case Half:
		x, y = int('h'-file)*size, int(rank-'1')*size
	}
	return
}

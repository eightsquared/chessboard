package chessboard

import (
	"fmt"
	"io"
	"math"
	"strconv"

	"gitlab.com/eightsquared/chess"
)

// WriteSVG renders a visual representation of the game g to w in SVG format.
func WriteSVG(w io.Writer, g chess.Game, o Options) (err error) {
	boardSize := o.Size & (^7)
	if boardSize < MinImageSize {
		boardSize = MinImageSize
	} else if boardSize > MaxImageSize {
		boardSize = MaxImageSize
	}
	rowSize := boardSize >> 3
	var (
		boardSizeStr = []byte(strconv.Itoa(boardSize))
		rowSizeStr   = []byte(strconv.Itoa(rowSize))
		coordStrs    = [8][]byte{
			[]byte(strconv.Itoa(0 * rowSize)),
			[]byte(strconv.Itoa(1 * rowSize)),
			[]byte(strconv.Itoa(2 * rowSize)),
			[]byte(strconv.Itoa(3 * rowSize)),
			[]byte(strconv.Itoa(4 * rowSize)),
			[]byte(strconv.Itoa(5 * rowSize)),
			[]byte(strconv.Itoa(6 * rowSize)),
			[]byte(strconv.Itoa(7 * rowSize)),
		}
		labelOffset     = rowSize >> 4
		labelXCoordStrs = [8][]byte{
			[]byte(strconv.Itoa(0*rowSize + labelOffset)),
			[]byte(strconv.Itoa(1*rowSize + labelOffset)),
			[]byte(strconv.Itoa(2*rowSize + labelOffset)),
			[]byte(strconv.Itoa(3*rowSize + labelOffset)),
			[]byte(strconv.Itoa(4*rowSize + labelOffset)),
			[]byte(strconv.Itoa(5*rowSize + labelOffset)),
			[]byte(strconv.Itoa(6*rowSize + labelOffset)),
			[]byte(strconv.Itoa(7*rowSize + labelOffset)),
		}
		labelYCoordStrs = [8][]byte{
			[]byte(strconv.Itoa(1*rowSize - labelOffset)),
			[]byte(strconv.Itoa(2*rowSize - labelOffset)),
			[]byte(strconv.Itoa(3*rowSize - labelOffset)),
			[]byte(strconv.Itoa(4*rowSize - labelOffset)),
			[]byte(strconv.Itoa(5*rowSize - labelOffset)),
			[]byte(strconv.Itoa(6*rowSize - labelOffset)),
			[]byte(strconv.Itoa(7*rowSize - labelOffset)),
			[]byte(strconv.Itoa(8*rowSize - labelOffset)),
		}
		lightRGBAStr = []byte(fmt.Sprintf("rgba(%d,%d,%d,%.2f)", o.Light.R, o.Light.G, o.Light.B, float64(o.Light.A)/255))
		darkRGBAStr  = []byte(fmt.Sprintf("rgba(%d,%d,%d,%.2f)", o.Dark.R, o.Dark.G, o.Dark.B, float64(o.Dark.A)/255))
	)
	dst := &svgWriter{w: w}
	dst.writeOpenTag(boardSize)
	dst.writeDefs(g, rowSize, boardSizeStr, rowSizeStr, lightRGBAStr, darkRGBAStr, o.Style)
	dst.writeGroupOpenTag(rowSize)
	dst.writeSquareBackground(coordStrs[0], coordStrs[0], dark)
	var x, y int
	var tx, ty []byte
	var p chess.Piece
	for s := chess.First; s <= chess.Last; s++ {
		x, y = rotate(s, o.Rotate, 1)
		if s.Light() {
			dst.writeSquareBackground(coordStrs[x], coordStrs[y], light)
		}
		lbl := label(s, o.Rotate)
		if lbl != "" {
			tx, ty = labelXCoordStrs[x], labelYCoordStrs[y]
			if s.Light() {
				dst.writeSquareLabel([]byte(lbl), tx, ty, darkRGBAStr)
			} else {
				dst.writeSquareLabel([]byte(lbl), tx, ty, lightRGBAStr)
			}
		}
		p = g.Board[s]
		if p == chess.Empty {
			continue
		}
		dst.writePiece(p, coordStrs[x], coordStrs[y])
	}
	dst.write(tagGroupClose)
	dst.write(tagSVGClose)
	err = dst.err
	return
}

type svgWriter struct {
	w      io.Writer
	err    error
	single []byte
}

var (
	tagSVG        = []byte(`<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"`)
	tagSVGClose   = []byte(`</svg>`)
	tagDefs       = []byte(`<defs>`)
	tagDefsClose  = []byte(`</defs>`)
	tagGroup      = []byte(`<g`)
	tagGroupClose = []byte(`</g>`)
	tagRect       = []byte(`<rect`)
	tagText       = []byte(`<text`)
	tagTextClose  = []byte(`</text>`)
	tagUse        = []byte(`<use`)
	tagUseClose   = []byte(`</use>`)
	attFill       = []byte(` fill="`)
	attFontFamily = []byte(` font-family="`)
	attFontSize   = []byte(` font-size="`)
	attHeight     = []byte(` height="`)
	attHref       = []byte(` xlink:href="`)
	attID         = []byte(` id="`)
	attTransform  = []byte(` transform="`)
	attWidth      = []byte(` width="`)
	attX          = []byte(` x="`)
	attY          = []byte(` y="`)
	quote         = []byte(`"`)
	slash         = []byte(`/`)
	bracket       = []byte(`>`)
	hash          = []byte(`#`)
	light         = []byte(`light`)
	dark          = []byte(`dark`)
	sansSerif     = []byte(`sans-serif`)
)

func (w *svgWriter) write(b []byte) {
	if w.err != nil {
		return
	}
	_, w.err = w.w.Write(b)
}

func (w *svgWriter) writeByte(b byte) {
	if w.err != nil {
		return
	}
	if w.single == nil {
		w.single = make([]byte, 1)
	}
	w.single[0] = b
	_, w.err = w.w.Write(w.single)
}

func (w *svgWriter) writeOpenTag(boardSize int) {
	size := []byte(strconv.Itoa(boardSize))
	w.write(tagSVG)
	w.write(attWidth)
	w.write(size)
	w.write(quote)
	w.write(attHeight)
	w.write(size)
	w.write(quote)
	w.write(bracket)
}

func (w *svgWriter) writeDefs(g chess.Game, rowSize int, boardSizeStr, rowSizeStr, lightRGBAStr, darkRGBAStr []byte, s PieceStyle) {
	w.write(tagDefs)
	w.write(tagRect)
	w.write(attID)
	w.write(light)
	w.write(quote)
	w.write(attWidth)
	w.write(rowSizeStr)
	w.write(quote)
	w.write(attHeight)
	w.write(rowSizeStr)
	w.write(quote)
	w.write(attFill)
	w.write(lightRGBAStr)
	w.write(quote)
	w.write(slash)
	w.write(bracket)
	w.write(tagRect)
	w.write(attID)
	w.write(dark)
	w.write(quote)
	w.write(attWidth)
	w.write(boardSizeStr)
	w.write(quote)
	w.write(attHeight)
	w.write(boardSizeStr)
	w.write(quote)
	w.write(attFill)
	w.write(darkRGBAStr)
	w.write(quote)
	w.write(slash)
	w.write(bracket)
	seen := make(map[chess.Piece]bool)
	var pieceSet map[chess.Piece][]byte
	var ok bool
	if pieceSet, ok = pieceVectors[s]; !ok {
		pieceSet = pieceVectors[DefaultStyle]
	}
	pieceScaleStr := []byte(fmt.Sprintf("scale(%f)", float64(rowSize)/512))
	var p chess.Piece
	for s := chess.First; s <= chess.Last; s++ {
		p = g.Board[s]
		if p == chess.Empty || seen[p] {
			continue
		}
		seen[p] = true
		w.write(tagGroup)
		w.write(attID)
		w.writeByte(byte(p.Rune()))
		w.write(quote)
		w.write(attTransform)
		w.write(pieceScaleStr)
		w.write(quote)
		w.write(bracket)
		w.write(pieceSet[p])
		w.write(tagGroupClose)
	}
	w.write(tagDefsClose)
}

func (w *svgWriter) writeGroupOpenTag(rowSize int) {
	fontSize := math.Min(MaxFontSize, float64(rowSize)/3.5)
	w.write(tagGroup)
	w.write(attFontFamily)
	w.write(sansSerif)
	w.write(quote)
	w.write(attFontSize)
	w.write([]byte(strconv.FormatFloat(fontSize, 'f', 2, 64)))
	w.write(quote)
	w.write(bracket)
}

func (w *svgWriter) writeSquareBackground(x, y, href []byte) {
	w.write(tagUse)
	w.write(attHref)
	w.write(hash)
	w.write(href)
	w.write(quote)
	w.write(attX)
	w.write(x)
	w.write(quote)
	w.write(attY)
	w.write(y)
	w.write(quote)
	w.write(bracket)
	w.write(tagUseClose)
}

func (w *svgWriter) writeSquareLabel(lbl []byte, x, y, fill []byte) {
	w.write(tagText)
	w.write(attX)
	w.write(x)
	w.write(quote)
	w.write(attY)
	w.write(y)
	w.write(quote)
	w.write(attFill)
	w.write(fill)
	w.write(quote)
	w.write(bracket)
	w.write(lbl)
	w.write(tagTextClose)
}

func (w *svgWriter) writePiece(p chess.Piece, x, y []byte) {
	w.write(tagUse)
	w.write(attHref)
	w.write(hash)
	w.writeByte(byte(p.Rune()))
	w.write(quote)
	w.write(attX)
	w.write(x)
	w.write(quote)
	w.write(attY)
	w.write(y)
	w.write(quote)
	w.write(bracket)
	w.write(tagUseClose)
}

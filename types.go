package chessboard

import (
	"image"
	"image/color"

	"gitlab.com/eightsquared/chess"
)

// Rotation enumerates the allowed rotational transforms that can be applied
// to a rendered board.
type Rotation uint8

// By default, no rotation is applied to the board. Clockwise and
// CounterCllockwise indicate that the board should be rotated one
// quarter-turn in the clockwise and counter-clockwise directions,
// respectively. Half indicates that the board should be rotated one
// half-turn, which effectively draws the board from Black's point of view.
const (
	None Rotation = iota
	Clockwise
	CounterClockwise
	Half
)

// PieceStyle enumerates the allowed piece styles.
type PieceStyle uint8

// Lasker is an open-source style found at
// https://gitlab.com/eightsquared/lasker. Merida is derived from a free font
// of the same name.
const (
	Lasker PieceStyle = iota
	Merida
	DefaultStyle PieceStyle = Lasker
)

// Options describes the options that may be applied to a rendered image.
type Options struct {
	// Size sets the image width and height in pixels.
	Size int
	// Light holds the color used to draw light-colored squares.
	Light color.RGBA
	// Dark holds the color used to draw dark-colored squares.
	Dark color.RGBA
	// Rotate describes how to transform (typically rotate) the board. (Piece
	// images and square labels are not affected by rotation.)
	Rotate Rotation
	// Style controls the look of the piece images.
	Style PieceStyle
}

// Defaults holds a set of reasonable default options.
var Defaults = Options{
	Size:   256,
	Light:  color.RGBA{255, 206, 158, 255},
	Dark:   color.RGBA{209, 139, 71, 255},
	Rotate: None,
	Style:  DefaultStyle,
}

// These constants establish bounds on the rendered image dimensions and the
// maximum label size.
const (
	MinImageSize = 8 * 16
	MaxImageSize = 8 * 256
	MaxFontSize  = 20
)

type font string
type size int
type pieceImageCache map[PieceStyle]map[chess.Piece]map[size]image.Image
type pieceVectorCache map[PieceStyle]map[chess.Piece][]byte

const (
	firaSans font = "Fira Sans"
)

const (
	small  size = 32
	medium size = 64
	large  size = 128
	xlarge size = 256
)

func (s PieceStyle) String() string {
	switch s {
	case Lasker:
		return "lasker"
	case Merida:
		return "merida"
	}
	return "unknown"
}

package chessboard

import (
	"gitlab.com/eightsquared/chess"
)

// label returns the text label for the square s when the board is rotated by
// the rotation r. A non-empty label is returned only if the square is in
// either the leftmost column or the bottommost row.
func label(s chess.Square, r Rotation) (label string) {
	x, y := rotate(s, r, 1)
	left, bottom := x == 0, y == 7
	if !left && !bottom {
		return
	}
	if left && bottom {
		label = string([]byte{byte(s.File()), byte(s.Rank())})
		return
	}
	if r == Clockwise || r == CounterClockwise {
		if left {
			label = string(s.File())
		} else {
			label = string(s.Rank())
		}
	} else {
		if left {
			label = string(s.Rank())
		} else {
			label = string(s.File())
		}
	}
	return
}

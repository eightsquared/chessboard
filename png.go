package chessboard

import (
	"image"
	"image/png"
	"io"
	"math"

	"github.com/golang/freetype"
	"golang.org/x/image/draw"
	"golang.org/x/image/math/fixed"

	"gitlab.com/eightsquared/chess"
)

// WritePNG renders a visual representation of the game g to w in PNG format.
func WritePNG(w io.Writer, g chess.Game, o Options) (err error) {
	boardSize := o.Size & (^7)
	if boardSize < MinImageSize {
		boardSize = MinImageSize
	} else if boardSize > MaxImageSize {
		boardSize = MaxImageSize
	}
	rowSize := boardSize >> 3
	pieceSize := pieceSizes[len(pieceSizes)-1]
	for i := len(pieceSizes) - 2; i >= 0 && int(pieceSizes[i]) > rowSize; i-- {
		pieceSize = pieceSizes[i]
	}
	dstBounds := image.Rectangle{Max: image.Point{boardSize, boardSize}}
	squareBounds := image.Rectangle{Max: image.Point{rowSize, rowSize}}
	dst := image.NewRGBA(dstBounds)
	var light, dark image.Image
	light = image.NewUniform(o.Light)
	dark = image.NewUniform(o.Dark)
	ctx := freetype.NewContext()
	ctx.SetDst(dst)
	ctx.SetFont(fonts[firaSans])
	ctx.SetFontSize(math.Min(MaxFontSize, float64(rowSize)/3.5))
	ctx.SetClip(dst.Bounds())
	scaledPieceCache := make(map[chess.Piece]*image.RGBA)
	var pieceCounts [256]uint8
	var pieceSet map[chess.Piece]map[size]image.Image
	var ok bool
	if pieceSet, ok = pieceImages[o.Style]; !ok {
		pieceSet = pieceImages[DefaultStyle]
	}
	for square := chess.First; square <= chess.Last; square++ {
		piece := g.Board[square]
		if piece == chess.Empty {
			continue
		}
		pieceCounts[piece]++
		if pieceCounts[piece] == 2 {
			g := pieceSet[piece][pieceSize]
			scaled := image.NewRGBA(squareBounds)
			draw.ApproxBiLinear.Scale(scaled, scaled.Bounds(), g, g.Bounds(), draw.Src, nil)
			scaledPieceCache[piece] = scaled
		}
	}
	for square := chess.First; square <= chess.Last; square++ {
		x, y := rotate(square, o.Rotate, rowSize)
		dr := image.Rectangle{image.Point{x, y}, image.Point{x + rowSize, y + rowSize}}
		if square.Light() {
			draw.Draw(dst, dr, light, image.Point{}, draw.Src)
		} else {
			draw.Draw(dst, dr, dark, image.Point{}, draw.Src)
		}
		lbl := label(square, o.Rotate)
		if lbl != "" {
			fx, fy := x+(rowSize>>4), y+rowSize-(rowSize>>4)
			if square.Light() {
				ctx.SetSrc(dark)
			} else {
				ctx.SetSrc(light)
			}
			p := fixed.Point26_6{X: fixed.Int26_6(fx << 6), Y: fixed.Int26_6(fy << 6)}
			if _, err = ctx.DrawString(lbl, p); err != nil {
				return
			}
		}
		if piece := g.Board[square]; piece != chess.Empty {
			if cached := scaledPieceCache[piece]; cached != nil {
				draw.Draw(dst, dr, cached, image.Point{}, draw.Over)
			} else {
				sourceImage := pieceSet[piece][pieceSize]
				draw.ApproxBiLinear.Scale(dst, dr, sourceImage, sourceImage.Bounds(), draw.Over, nil)
			}
		}
	}
	err = png.Encode(w, dst)
	return
}

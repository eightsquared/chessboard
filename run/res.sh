#!/bin/bash

err () {
	(>&2 echo $@)
	popd
	exit 1
}

run_svgo () {
	local style="$1"
	svgo \
		--quiet \
		-f "../res/$style/svg/src" \
		-o "../res/$style/svg" \
		--enable=removeAttrs \
		--config=./svgo.yml
}

run_inkscape () {
	local style="$1"
	mkdir -p "../res/$style/png"
	for size in 256 128 64 32; do
		echo -n "$size..."
		for file in $(ls -1 "../res/$style/svg/"); do
			if [[ -f "../res/$style/svg/$file" ]]; then
				inkscape \
					-w $size -h $size \
					--export-area-page \
					--export-png="../res/$style/png/$(basename $file .svg)_$size.png" \
					"../res/$style/svg/$file" &> /dev/null || exit 1
			fi
		done
	done
}

run_pngcrush () {
	local style="$1"
	for file in $(ls -1 "../res/$style/png/"); do
		if [[ -f "../res/$style/png/$file" ]]; then
			pngcrush \
				-quiet \
				"../res/$style/png/$file" \
				"../res/$style/png/crushed_$file" &> /dev/null || exit 1
			mv "../res/$style/png/crushed_$file" "../res/$style/png/$file"
		fi
	done
}

pushd $(dirname $0) &> /dev/null

svgo -v &> /dev/null || err "this script requires svgo: npm install --global svgo"
inkscape --version &> /dev/null || err "this script requires inkscape: https://inkscape.org/download"
pngcrush -version &> /dev/null || err "this script requires pngcrush: https://pmt.sourceforge.io/pngcrush"

for style in $(ls -1 ../res); do
	echo -n "$style: optimizing svg sources..."
	rm -f "../res/$style/svg/*.svg"
	run_svgo "$style" || err "svgo failed"
	echo "done."
	echo -n "$style: exporting png images..."
	rm -f "../res/$style/png/*.png"
	run_inkscape "$style" || err "inkscape failed"
	echo "done."
	echo -n "$style: optimizing png images..."
	run_pngcrush "$style" || err "pngcrush failed"
	echo "done."
done

popd &> /dev/null

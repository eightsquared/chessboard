/*
Package chessboard provides functions to render a chess position as an image.

To use this package, you will probably also need the chess package
(https://gitlab.com/eightsquared/chess) to describe games. For example,
supposing you have the following board:

	// White to mate in one move.
	g := chess.Game{
		Board: chess.Board{
			chess.E8: chess.BlackBishop,
			chess.F8: chess.BlackKing,
			chess.A7: chess.BlackPawn,
			chess.B7: chess.BlackPawn,
			chess.C7: chess.BlackPawn,
			chess.G7: chess.BlackPawn,
			chess.C6: chess.BlackKnight,
			chess.D6: chess.BlackPawn,
			chess.H6: chess.BlackPawn,
			chess.C5: chess.BlackBishop,
			chess.C4: chess.WhiteBishop,
			chess.F4: chess.WhiteBishop,
			chess.G4: chess.BlackKnight,
			chess.H4: chess.BlackQueen,
			chess.C3: chess.WhiteKnight,
			chess.A2: chess.WhitePawn,
			chess.B2: chess.WhitePawn,
			chess.G2: chess.WhitePawn,
			chess.H2: chess.WhitePawn,
			chess.E1: chess.WhiteRook,
			chess.F1: chess.WhiteRook,
			chess.H1: chess.WhiteKing,
		},
		ToMove: chess.White,
	}

You can write an ASCII-art visualization of this position to standard output as
follows:

	err := chessboard.WriteSVG(os.Stdout, chessboard.Defaults)
	// Output:
	//
	//   ╔═══╤═══╤═══╤═══╤═══╤═══╤═══╤═══╗
	// 8 ║   │   │   │   │ b │ k │   │   ║
	//   ╟───┼───┼───┼───┼───┼───┼───┼───╢
	// 7 ║ p │ p │ p │   │   │   │ p │   ║
	//   ╟───┼───┼───┼───┼───┼───┼───┼───╢
	// 6 ║   │   │ n │ p │   │   │   │ p ║
	//   ╟───┼───┼───┼───┼───┼───┼───┼───╢
	// 5 ║   │   │ b │   │   │   │   │   ║
	//   ╟───┼───┼───┼───┼───┼───┼───┼───╢
	// 4 ║   │   │ B │   │   │ B │ n │ q ║
	//   ╟───┼───┼───┼───┼───┼───┼───┼───╢
	// 3 ║   │   │ N │   │   │   │   │   ║
	//   ╟───┼───┼───┼───┼───┼───┼───┼───╢
	// 2 ║ P │ P │   │   │   │   │ P │ P ║
	//   ╟───┼───┼───┼───┼───┼───┼───┼───╢
	// 1 ║   │   │   │   │ R │ R │   │ K ║
	//   ╚═══╧═══╧═══╧═══╧═══╧═══╧═══╧═══╝
	// w   a   b   c   d   e   f   g   h

In addition to ASCII output, this package supports rendering to SVG (fast) and
PNG (much slower due to raster scaling operations).
*/
package chessboard

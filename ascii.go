package chessboard

import (
	"io"

	"gitlab.com/eightsquared/chess"
)

// WriteASCII renders a visual representation of the game g to w in ASCII art
// format.
//
// This function does not yet support board rotation.
func WriteASCII(w io.Writer, g chess.Game, o Options) (err error) {
	dst := &asciiWriter{w: w}
	for s := chess.First; s <= chess.Last; s++ {
		f := s.File()
		if s == chess.A8 {
			dst.write(asciiTopRow)
			dst.write(newline)
		} else if f == 'a' {
			dst.write(asciiInterRow)
			dst.write(newline)
		}
		if f == 'a' {
			dst.writeByte(byte(s.Rank()))
			dst.write(space)
			dst.write(asciiDoubleVertical)
		} else {
			dst.write(asciiSingleVertical)
		}
		p := g.Board[s]
		if (p.Is(chess.WhiteKing) && g.Castling.Allows(chess.WhiteQueenside)) ||
			(p.Is(chess.BlackKing) && g.Castling.Allows(chess.BlackQueenside)) {
			dst.write(leftBracket)
		} else {
			dst.write(space)
		}
		dst.writeByte(byte(g.Board[s].Rune()))
		if (p.Is(chess.WhiteKing) && g.Castling.Allows(chess.WhiteKingside)) ||
			(p.Is(chess.BlackKing) && g.Castling.Allows(chess.BlackKingside)) {
			dst.write(rightBracket)
		} else {
			dst.write(space)
		}
		if f == 'h' {
			dst.write(asciiDoubleVertical)
			dst.write(newline)
		}
	}
	dst.write(asciiBottomRow)
	dst.write(newline)
	dst.writeByte(byte(g.ToMove.Color().Rune()))
	dst.write(asciiFiles)
	dst.write(newline)
	err = dst.err
	return
}

type asciiWriter struct {
	w      io.Writer
	err    error
	single []byte
}

var (
	asciiTopRow         = []byte("  ╔═══╤═══╤═══╤═══╤═══╤═══╤═══╤═══╗")
	asciiInterRow       = []byte("  ╟───┼───┼───┼───┼───┼───┼───┼───╢")
	asciiBottomRow      = []byte("  ╚═══╧═══╧═══╧═══╧═══╧═══╧═══╧═══╝")
	asciiFiles          = []byte("   a   b   c   d   e   f   g   h  ")
	asciiSingleVertical = []byte("│")
	asciiDoubleVertical = []byte("║")
	space               = []byte(" ")
	newline             = []byte("\n")
	leftBracket         = []byte("<")
	rightBracket        = []byte(">")
)

func (w *asciiWriter) write(b []byte) {
	if w.err != nil {
		return
	}
	_, w.err = w.w.Write(b)
}

func (w *asciiWriter) writeByte(b byte) {
	if w.err != nil {
		return
	}
	if w.single == nil {
		w.single = make([]byte, 1)
	}
	w.single[0] = b
	_, w.err = w.w.Write(w.single)
}

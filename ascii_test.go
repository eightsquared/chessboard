package chessboard

import (
	"io/ioutil"
	"testing"

	"gitlab.com/eightsquared/chess"
)

func BenchmarkWriteASCII(b *testing.B) {
	positions := make([]chess.Game, 1e5)
	for i := range positions {
		positions[i] = chess.RandomGame(i)
	}
	options := Defaults
	b.Run("bench", func(b *testing.B) {
		var err error
		for i := 0; i < b.N; i++ {
			err = WriteASCII(ioutil.Discard, positions[i%len(positions)], options)
			if err != nil {
				b.Fatal(err)
			}
		}
	})
}

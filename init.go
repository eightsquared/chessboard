package chessboard

//go:generate esc -o=static.go -pkg=chessboard -private res

import (
	"bytes"
	"fmt"
	"image"
	"image/png"
	"strings"

	"github.com/golang/freetype"
	"github.com/golang/freetype/truetype"

	"gitlab.com/eightsquared/chess"
)

var (
	pieceImages  pieceImageCache
	pieceVectors pieceVectorCache
	pieceStyles  = []PieceStyle{Merida, Lasker}
	pieceSizes   = []size{small, medium, large, xlarge}
	pieceNames   = map[chess.Piece]string{
		chess.WhitePawn:   "white_pawn",
		chess.WhiteKnight: "white_knight",
		chess.WhiteBishop: "white_bishop",
		chess.WhiteRook:   "white_rook",
		chess.WhiteQueen:  "white_queen",
		chess.WhiteKing:   "white_king",
		chess.BlackPawn:   "black_pawn",
		chess.BlackKnight: "black_knight",
		chess.BlackBishop: "black_bishop",
		chess.BlackRook:   "black_rook",
		chess.BlackQueen:  "black_queen",
		chess.BlackKing:   "black_king",
	}
	fonts map[font]*truetype.Font
)

func init() {
	pieceImages = buildImageCache()
	pieceVectors = buildVectorCache()
	fonts = map[font]*truetype.Font{
		firaSans: loadFont("FiraSans-Regular"),
	}
}

func buildImageCache() pieceImageCache {
	cache := make(pieceImageCache)
	for _, style := range pieceStyles {
		cache[style] = make(map[chess.Piece]map[size]image.Image)
		for piece, name := range pieceNames {
			cache[style][piece] = make(map[size]image.Image)
			for _, size := range pieceSizes {
				f := fmt.Sprintf("/res/%s/png/%s_%d.png", style, name, size)
				b := _escFSMustByte(false, f)
				var err error
				cache[style][piece][size], err = png.Decode(bytes.NewReader(b))
				if err != nil {
					panic(fmt.Errorf("error building image cache: %v", err))
				}
			}
		}
	}
	return cache
}

func buildVectorCache() pieceVectorCache {
	cache := make(pieceVectorCache)
	for _, style := range pieceStyles {
		cache[style] = make(map[chess.Piece][]byte)
		for piece, name := range pieceNames {
			f := fmt.Sprintf("/res/%s/svg/%s.svg", style, name)
			s := strings.TrimSpace(_escFSMustString(false, f))
			s = s[87 : len(s)-6] // eliminate opening and closing <svg> tag
			cache[style][piece] = []byte(s)
		}
	}
	return cache
}

func loadFont(name string) *truetype.Font {
	b := _escFSMustByte(false, fmt.Sprintf("/res/font/%s.ttf", name))
	f, err := freetype.ParseFont(b)
	if err != nil {
		panic(fmt.Errorf("error loading font %s: %v", name, err))
	}
	return f
}

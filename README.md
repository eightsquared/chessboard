# chessboard

A [Go](https://golang.org) library for rendering chess positions described by [package chess](https://gitlab.com/eightsquared/chess) as images.

## Dependencies

This package does not currently use dependency management, so you may need to manually `go get` all dependencies prior to building.

- [gitlab.com/eightsquared/chess](https://gitlab.com/eightsquared/chess)
- [github.com/golang/freetype](https://github.com/golang/freetype)
- [golang.org/x/image/draw](https://github.com/golang/image)
- [golang.org/x/image/math/fixed](https://github.com/golang/image)

## Documentation

[![API Reference](https://godoc.org/gitlab.com/eightsquared/chessboard?status.svg)](https://godoc.org/gitlab.com/eightsquared/chessboard)

## Installation

```
go get -u gitlab.com/eightsquared/chessboard
```

## License

[MIT](LICENSE)
